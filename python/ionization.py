import numpy as np
import matplotlib.pyplot as plt

UNTIL = 40

with open('ionization_data.csv', 'r') as file:
	data = file.readlines()
	
energies = np.array([int(data[x].rstrip().split(',')[0]) for x in range(len(data))])
Z = np.array([int(data[x].rstrip().split(',')[3]) for x in range(len(data))])
element = np.array([data[x].rstrip().split(',')[2] for x in range(len(data))])


idx = np.argsort(Z)

Z = np.array(Z)[idx]
energies = np.array(energies)[idx] * 1e-5
element = np.array(element)[idx]

plt.xlabel(r'Ordnungszahl $A$', fontsize=16)
plt.ylabel(r'1. Ionisierungsenergie $E$ / \si{\mega\joule\per\mol}', fontsize=16)

plt.plot(Z[:UNTIL], energies[:UNTIL], '-o', linewidth=1, color='black', markersize=3)

for i in range(1, len(energies[:UNTIL]), 2):
	plt.text(Z[i], energies[i] + 0.05, element[i], horizontalalignment='center', bbox=dict(facecolor='white', alpha=0.5))

plt.tight_layout()
plt.savefig("ionization.pdf")
